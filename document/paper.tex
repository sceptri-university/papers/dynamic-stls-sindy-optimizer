\documentclass[preprint,12pt]{elsarticle}

\input{includes/preambule.tex}
\input{includes/math.tex}

\journal{Journal of Nonlinear Science}

\begin{document}

\input{includes/lua_macros.tex}

\begin{frontmatter}
	%% Title, authors and addresses

	\title{Dynamic STLS SINDy Optimizer}

	\affiliation[inst1]{
		organization={Masaryk University, Department of Mathematics and Statistics},
		addressline={Kotlářská 2}, 
		city={Brno},
		postcode={61237}, 
		country={Czech Republic}
	}

	%# TODO: Is the order correct? I really don't know :(
	\author[inst1]{Štěpán Zapadlo}
	\author[inst1]{Lenka Přibylová}

	%# TODO [ACA-16]: Write abstract
	\begin{abstract}
		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	\end{abstract}

	%# TODO [ACA-17]: Fix/Add Graphical abstract, research highlights and more
	%% Graphical abstract
	\begin{graphicalabstract}
		\includegraphics{graphical-abstract}
	\end{graphicalabstract}

	%% Research highlights
	\begin{highlights}
		\item Research highlight 1
		\item Research highlight 2
	\end{highlights}

	\begin{keyword}
		keyword one \sep keyword two
		\PACS 0000 \sep 1111
		\MSC 0000 \sep 1111
	\end{keyword}
\end{frontmatter}

%% \linenumbers

%% main text
\section{Introduction}
\label{sec:introduction}
Scientists continuously strive to unravel the intricacies of the world around us, gathering a myriad of data related to their specific fields of research, either from meticulous experiments or comprehensive studies. Neuroscientists measure local field potentials from a specific brain sample \cite{Pail2020}, and aerospace engineers investigate the interplay between drag, airspeed, and acceleration around a wing design. While raw data provides a snapshot of these systems, understanding the intrinsic dynamics propelling these observations is paramount. Consequently, a plethora of methodologies has emerged, dedicated to deciphering the nonlinear dynamics inherent in such datasets. Predominantly, these methods aim to deduce an encompassing system of differential equations from the acquired numerical measurements.

This paper extends methods used for Sparse Identification of Nonlinear Dynamics (SINDy), which is the work of Brunton et al. from 2016 \cite{BruntonSINDyCore,brunton_kutz_2019}. 
%# TODO: Reword (way too long sentences)
SINDy is a machine-learning approach that addresses the presented problem by using sparse regression to select the optimal library of candidate terms for describing dynamics. Even though neural networks (NNs) have recently become very popular, primarily due to their universal approximation capabilities \cite{HORNIK1989359, Goodfellow-et-al-2016}, their inability to extrapolate, integrate existing knowledge such as the laws of physics, or the black-box nature of the NN models can be limitations. In this context, the relatively simple approach of SINDy, which employs sparsity-promoting linear regression, becomes all the more appealing \cite{champion2020unified}. There are also other methods like \textit{Symbolic Regression} \cite{Schmidt2009}, \textit{Automatic Inference of Dynamics} \cite{Daniels2015} or a novel method using \textit{Arithmetic Neural Networks}~\cite{HeimANN2020}.

Moreover, we can use SINDy with control variables \cite{Kaiser_2018}, explicitly stated parameters \cite{rudy2018datadriven} or even to identify partial differential equations \cite{rudy2016datadriven}. The diversity of use cases suggests that likely no single method for solving the underlying optimization problem will be optimal for all situations. This is the main reason for our proposed method \textit{Dynamic Sequentially Thresholded Least Squares} (DSTLS), which is in and of itself a modification of \textit{Sequentially Thresholded Least Squares} (STLS), a method revealed and recommended by Bruton et al. in \cite{BruntonSINDyCore}. While STLS was in some ways superseded by \textit{sparse relaxed regularized regression} (SR3) \cite{zheng2018unified, champion2020unified}, we found our novel method more capable in certain scenarios.

\section{Problem formulation}
\label{sec:problem_formulation}

SINDy is a method of discovering underlying nonlinear dynamical systems from numerical data originating from these systems. We will focus exclusively on learning systems of ordinary differential equations (ODEs). Thus we presume the studied system may be written in the form
$$
	\dvi x(t) = \vi f (\vi x(t)), \quad \vi f : \R^l \to \R^l.
$$

It is also a good moment to point out our assumption that the studied system is autonomous, i.e. the right-hand side is solely a function of the state $\vi x$ and the dynamics don't change with time. In general, the function $\vi f$ is an arbitrary mapping from $\R^l$ to $\R^l$, but to be able to use SINDy which is based on sparsity promoting linear regression, we must further restrict it. Therefore our next assumption is that $\vi f$ may be approximated as
$$
	\vi f(\vi x) \approx \mtr{
		\theta_1(\vi x) \xi_{1,1} + \dots + \theta_p(\vi x) \xi_{p,1} \\
		\vdots \\
		\theta_1(\vi x) \xi_{1,l} + \dots + \theta_p(\vi x) \xi_{p, l}
	}\Tr = \vi \Theta(\vi x) \vi \Xi,
$$
where $\vi \Theta(\vi x) = \mtr{\theta_1(\vi x), \dots, \theta_p(\vi x)}$ for $p \in \N, \forall i \in \set{1, \dots, p}: \theta_i : \R^l \to \R$ and $\vi \Xi \in \R^{p \times l}$. In other words, we are trying to approximate arbitrary function $\vi f$ with \textit{candidate (or basis) functions} $\theta_i$, such that $i$-th coordinate of $\vi f$ is expressed by a sum of candidates $\theta_i$ weighted by appropriate parameters.

\subsection{Koopman operator theory}
\label{sec:koopman}
%# TODO: Does this even make sense? 

For a deeper comprehension of the rationale behind this assumption, one may refer to the principles elucidated in the Koopman operator theory \cite{KoopmanOriginal, KoopmanSpectralAnalysis}. Koopman theory studies an operator $\Koop_t$ that evolves a measurement function $g: \R^l \to \R$ of the state of the system for some arbitrary time step \cite{brunton_kutz_2019}. For our system, this looks like
$$
	\Koop_t g = g \circ \vi f,
$$
which gives a linear operator in an infinite-dimensional Hilbert space. For a discrete-time system with a given fixed time $\Delta t$ we get
$$
	\Koop_{\Delta t} g(\vi x_k) = g(\vi f(\vi x_k)) = g(\vi x_{k+1}).
$$

In other words, the Koopman operator defines an infinite-dimensional linear dynamical system \cite{brunton_kutz_2019}, which propagates a measurement $g_k$ of the state $\vi x_k$, computed as $g_k = g(\vi x_k)$, in time.

While the Koopman operator is infinite-dimensional, we can approximate using finite dimensions. This translates to choosing basis functions and finding the right parameters for those functions, such that this linear combination best advances the current measurement in time, i.e. must approximate an eigenfunction of the Koopman operator for our system \cite{kaiser2021datadriven}.

\subsection{Sparse regression}
\label{sec:sparse_regression}

Let's now assume we selected candidate functions $\theta_1, \dots, \theta_p$ and also possess state measurements as well as derivatives, i.e.
$$
	\vi X = \mtr{\vi x_1\\ \vdots\\ \vi x_n}, \quad \dvi X = \mtr{\dvi x_1 \\ \vdots \\ \dvi x_n},
$$
such that $\vi x_i = \vi x(t_i)$ and $\dvi x_i = \dvi x(t_i)$. Now we can enhance our state measurements by our candidate functions
$$
	\vi \Theta(\vi X) = \mtr{
		\theta_1(\vi x_1) & \dots & \theta_p(\vi x_1) \\
		\vdots & \ddots & \vdots \\
		\theta_1(\vi x_n) & \dots & \theta_p(\vi x_n)
	}.
$$

This can be furthermore written as a $l$-dimensional linear model
$$
\dvi X = \vi \Theta(\vi X) \vi \Xi + \vi \epsilon,
$$
which we try to solve under the constraint of \textit{parsimony}, meaning we try to select a model that fits the data reasonably well with as many zero coefficients as possible. 

In general, finding the coefficients of $l$-dimensional linear model means solving the following optimization problem
\begin{equation}
	\label{eq:l_dimensional_lsq}
	\min_{\vi \Xi} \frac 1 2 \norm{\dvi X - \vi \Theta(\vi X)\vi \Xi}_2^2.
\end{equation}

While this approach will give us an optimal solution, found model will not be parsimonious. Therefore we have to introduce a constraint to enforce parsimony. One possible way is to add regularization term $R : \R^{p \times l} \to \R$ promoting sparsity with parameter $\mu$
$$
	\min_{\vi \Xi} \brackets{\frac 1 2 \norm{\dvi X - \vi \Theta(\vi X)\vi \Xi}_2^2 + \mu R(\vi \Xi)}.
$$

By choosing $R = \norm{\cdot}_1$ we get a LASSO regression \cite{LassoRegression}. On the other hand, we can also require a minimum size of any of the coefficients, which will in turn give us a sparse matrix $\vi \Xi$. This can be written as
\begin{equation}
	\label{eq:stls_formulation}	
	\begin{gathered}
		\min_{\vi \Xi} \frac 1 2 \norm{\dvi X - \vi \Theta(\vi X)\vi \Xi}_2^2\\
		\constraint \forall i \in \set{1, \dots, p}, j \in \set{1, \dots, l}: \quad \absval{\Xi_{i,j}} \geq \tau,
	\end{gathered}
\end{equation}
where $\tau$ is the threshold for the coefficient size. This is usually solved with \textit{Sequentially Thresholded Least Squares} (STLS) method.

\section{Dynamic Sequentially Thresholded Least Squares}

In the previous section, we briefly mentioned STLS method for solving $l$-dimensional linear regression with sparsity (or more precisely parsimony) constraint. We might notice that the problem \eqref{eq:stls_formulation} can be solved in parts, each corresponding to a variable (column) of the derivative matrix $\dvi X$. Let $k \in \set{1, \dots, l}$, then for the $k$-th variable our problem becomes
\begin{equation*}
	\label{eq:each_stls_formulation}	
	\begin{gathered}
		\min_{\vi \xi_k} \frac 1 2 \norm{\dvi X_{\cdot, k} - \vi \Theta(\vi X)\vi \xi_k}_2^2\\
		\constraint \forall i \in \set{1, \dots, p}: \quad \absval{{\xi_k}_i} \geq \tau.
	\end{gathered}
\end{equation*}

%# TODO: Clean up this mess
This transformation reduces an $l$-dimensional constrained linear regression into a series of $l$ 1-dimensional constrained linear regressions. For the sake of simplicity, we will write
\begin{equation}
	\label{eq:each_stls_formulation_simple}	
	\begin{gathered}
		\min_{\vi \xi} \frac 1 2 \norm{\dvi x - \vi \Theta \vi \xi}_2^2\\
		\constraint \forall i \in \set{1, \dots, p}: \quad \absval{\xi_i} \geq \tau,
	\end{gathered}
\end{equation}
where $\dvi x = \dvi X_{\cdot, k}, \vi \Theta= \vi \Theta(\vi X)$ and $\vi \xi = \vi \xi_k = \vi \Xi_{\cdot, k}$. This rather simplified notation allows us to more easily express the iterative scheme used to solve the problem \eqref{eq:each_stls_formulation_simple}:
\begin{equation}
	\label{eq:stls_iterative}
	\begin{gathered}
		\vi \xi^0 = \vi \Theta \Pinv \dvi X_{\cdot, k}, \\
		S^j = \set{m \in \oneToN{p} : \absval{\xi_m^j} \geq \tau}, \quad j \in \N_0, \\
		\vi \xi^{j+1} = \argmin_{\vi \xi \in \R^p : \supp(\vi \xi) \subseteq S^j} \frac 1 2 \norm{\dvi x - \vi \Theta \vi \xi}_2^2, \quad j \in \N_0,
	\end{gathered}
\end{equation}
such that $\supp(\vi \xi) = \set{m \in \oneToN{p} : \xi_m \neq 0}$ is called a support set of $\vi \xi$ and $\oneToN{n}$ denotes a set $\set{1, \dots, n}$. Convergence of \eqref{eq:stls_iterative} in the algorithmic sense as well as convergence to the local minimizer of the problem was proven in \cite{sindyConvergence}.

Let us now focus on the problems with STLS, which will naturally lead us to its modification. First, consider the ODE FitzHugh-Nagumo model \cite{FitzHugh}
\begin{equation}
	\label{eq:fhn}
	\begin{gathered}
		\dot V = 0.8 - W + V - \frac 1 3 V^3, \\
		\dot W =  0.056 - 0.064 \cdot W + 0.08 \cdot V,
	\end{gathered}
\end{equation}
where we can see that the expression for $\dot V$ has parameters in the hundredths, but for $\dot W$ they are in the thousandths. Now if we were to choose a suitable $\tau$ for the discovery of the system \eqref{eq:fhn} from noisy data, we would very probably run into an issue where choosing a small enough threshold $\tau$, e.g. $\tau \leq 0.03$, would allow us to discover correct dynamics in $W$, but the fitted expression for $\dot V$ would contain unnecessary small coefficients (and additional terms with them). But choosing $\tau$ bigger, e.g. $\tau \geq 0.1$, to get rid of those unwanted small terms in $V$ has the effect that $\dot W$ is fitted to a constant $0$.

To mitigate problems with the size discrepancy between coefficients in equations we propose a novel method \textit{Dynamic Sequentially Thresholded Least Squares} (DSTLS). DSTLS is a modification of STLS where coefficients in expressions for each variable of the $l$-dimensional least squares \eqref{eq:l_dimensional_lsq} are thresholded dynamically with respect to the values of already found coefficients. This transforms STLS's optimization problem \eqref{eq:each_stls_formulation_simple} into 
\begin{equation}
	\label{eq:dstls}	
	\begin{gathered}
		\min_{\vi \xi} \frac 1 2 \norm{\dvi x - \vi \Theta \vi \xi}_2^2\\
		\constraint \forall i \in \set{1, \dots, p}: \quad \absval{\xi_i} \geq \tau \cdot \max \absval{\vi \xi},
	\end{gathered}
\end{equation}
where $\max \absval{\vi \xi} = \max \set{\absval{\xi_k} : \xi_k \in \vi \xi}$. This changes rewards coefficients, which are larger in absolute value while also making the threshold value relative to each iteration and variable of the derivatives matrix $\dvi X$.

DSTLS formulated as \eqref{eq:dstls} then leads to a slightly different iterative scheme from \eqref{eq:stls_iterative}
\begin{subequations}
	\label{eq:dstls_iterative}
	\begin{gather}
		\vi \xi^0 = \vi \Theta \Pinv \dvi X_{\cdot, k}, \label{eq:dstls_iterative:init} \\
		S^j = \set{m \in \oneToN{p} : \absval{\xi_m^j} \geq \tau \cdot \max \absval{\vi \xi^j}}, \quad j \in \N_0, \label{eq:dstls_iterative:set} \\
		\vi \xi^{j+1} = \argmin_{\vi \xi \in \R^p : \supp(\vi \xi) \subseteq S^j} \frac 1 2 \norm{\dvi x - \vi \Theta \vi \xi}_2^2, \quad j \in \N_0. \label{eq:dstls_iterative:step}
	\end{gather}
\end{subequations}

We will now prove an analogous statement as in \cite{sindyConvergence}.

\begin{theorem}
	The iterative scheme defined by \eqref{eq:dstls_iterative} converges in at most $p$ steps. 
\end{theorem}

\begin{proof}
	As denoted in \eqref{eq:dstls_iterative:init}, let $\set{\vi \xi^j}_{j = 0}^L$, where $L \in \N \cup \set{\infty}$, be the sequence generated by \eqref{eq:dstls_iterative}. From the \eqref{eq:dstls_iterative:step} follows that surely $\supp (\vi \xi^{j+1}) \subseteq S^j$. Furthermore \eqref{eq:dstls_iterative:set} dictates $S^{j+1} \subseteq \supp {\vi \xi^{j+1}}$. The two previous statements combined imply the sets $S^j$ are nested
	\begin{equation}
		\label{eq:dstls_iteraive:proof_nested}
		S^{j+1} \subseteq \supp(\vi \xi^{j+1}) \subseteq S^j.
	\end{equation}

	Let's now assume there exists an integer $J \in \N_0$ such that $S^{J + 1} = S^J$ and $S^J \neq \emptyset$. Applying this to \eqref{eq:dstls_iterative:step} we get
	$$
		\vi \xi^{J+2} = 
		\argmin_{\supp(\vi \xi) \subseteq S^{J + 1}} \frac 1 2 \norm{\dvi x - \vi \Theta \vi \xi}_2^2 = 
		\argmin_{\supp(\vi \xi) \subseteq S^J} \frac 1 2 \norm{\dvi x - \vi \Theta \vi \xi}_2^2 = 
		\vi \xi^{J+1}.
	$$

	Therefore in this case, $\vi \xi^j = \vi \xi^{J+1}$ for all $j \geq J+1$ and trivially from the initial assumption $S^j = S^{J + 1}$ for all $j \geq J$. Since cardinality $\card (S^j) \leq p$ for all $j \in \N_0$, we can deduce that $J \leq p$ and as such, the scheme \eqref{eq:dstls_iterative} converges in at most $p$ steps.

	We shall now consider the second case, where there does not exist an integer $J \in \N$ such that $S^{J + 1} = S^J$ and $S^J \neq \emptyset$. This implies the sets $S^j$ are strictly nested, otherwise the first case would happen, therefore
	$$
		S^{j+1} \subsetneq S^j \quad \text{for all }j\text{ such that } S^j \neq \emptyset.
	$$

	From $\card(S^j) \leq p$ for all $j \in \N$ follows that $S^j = \emptyset$ for $j > p$ and the scheme \eqref{eq:dstls_iterative} converges to an empty set within $p$ iterations.
\end{proof}	

\section{Applications of DSTLS}

\codeSnippet{juliacode}{fhn_generator.jl}

\bibliographystyle{elsarticle-num} 
\bibliography{paper}

\end{document}
\endinput