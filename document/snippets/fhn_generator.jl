using Revise
using ModelingToolkit, DifferentialEquations, Random

include("../src/custom.jl")
using .Custom

# Setting up independent variable namely time t and dependent variable V(t), W(t)
@variables t V(t) W(t)
@parameters a b c d iₑ

D = Differential(t)

# Governing equations for our system
include(here("../snippets/fhn.jl"))

params = [
    a => 0.08, b => 1.0,
    c => 0.8, d => 0.7,
    iₑ => 0.8
]
starting_values = [3.3, -2.0]
time_range = (0.0, 100.0)
# Add prolonged time range
time_rangeₗ = (0.0, 200.0)

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(V) ~ V̇(V, W, a, b, c, d, iₑ),
    D(W) ~ Ẇ(V, W, a, b, c, d, iₑ)
])
ode_problem = ODEProblem(ode_system, starting_values, time_range, params)
ode_problemₗ = ODEProblem(ode_system, starting_values, time_rangeₗ, params)

saveat = 0.1

# The ODE problem may be stiff, so use a stiff solver
solution = solve(ode_problem, Rosenbrock23(); saveat)
solutionₗ = solve(ode_problemₗ, Rosenbrock23(); saveat)

# transform vectors of states and derivatives into matrices
X = vcat(reshape.(solution.u, 1, 2)...)
times = solution.t

Xₗ = vcat(reshape.(solutionₗ.u, 1, 2)...)
timesₗ = solutionₗ.t