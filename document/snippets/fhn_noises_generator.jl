using Revise
using ModelingToolkit, DifferentialEquations
using CairoMakie, LaTeXStrings
#using DataFrames, CSV, Random
using Random

include("../src/custom.jl")
using .Custom

Random.seed!(123)

@variables t V(t) W(t)
@parameters a b c d iₑ

# Our system is one ODE, so we only need differential with respect to time
D = Differential(t)

# Governing equation(s) for our system
include(Custom.here("../snippets/fhn.jl"))

params = [
    a => 0.08, b => 1.0,
    c => 0.8, d => 0.7,
    iₑ => 0.8
]
starting_values = [3.3, -2.0]
full_time = (0.0, 100.0)

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(V) ~ V̇(V, W, a, b, c, d, iₑ),
    D(W) ~ Ẇ(V, W, a, b, c, d, iₑ)
])

# Solve the system for a long time to get a trajactory, from which we can sample
# desired initial conditions
saveat = 0.01
ode_problem = ODEProblem(ode_system, starting_values, full_time, params)
solution = solve(ode_problem, Tsit5(); saveat)

time_range = (0.0, 100.0)
ic_count = 10
initial_conditions = rand(solution.u, ic_count)

ode_problem = ODEProblem(ode_system,
    initial_conditions[1],
    time_range,
    params
)

"""
EnsembleProblem function, which allows us to change starting value based on current index
"""
function problem_function(problem, index, repeat)
    @. problem.u0 = initial_conditions[index]
    problem
end

ensemble_problem = EnsembleProblem(ode_problem; prob_func=problem_function)
solution = solve(ensemble_problem; saveat, trajectories=length(initial_conditions))

results = Custom.process_solution(solution, 
	(:V, :W),
    (coords, t) -> [
        V̇(coords..., last.(params)...),
        Ẇ(coords..., last.(params)...)
    ]
)