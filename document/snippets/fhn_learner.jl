using Revise
using ModelingToolkit, DifferentialEquations, Random
using DynamicSTLS
using NoiseRobustDifferentiation
using DataDrivenDiffEq, DataDrivenSparse
using CairoMakie

include("../src/custom.jl")
using .Custom, .Custom.Noiser

# Generate necessary data - stored only in variables
# include("fhn_generator.jl")

Random.seed!(123)

# Noise the data
noise = AdditiveVarianceNoise(0, 0.00)
Xₙ = noise(X)

# And denoise it afterwards
λ = [0.3, 0.3];
k = [4, 5];
X₁ = denoise(Xₙ', k, λ)

# But compute the derivative from noisy data
resolution = length(times)
time_length = times[end] - times[1]
each_step = time_length / resolution

tvdiff_parameters = (dx=each_step, scale="small", ε=1e-3)
number_of_iterations = [30, 30]
regularization = [0.08, 0.9]
Ẋ = tvdiff(Xₙ', number_of_iterations, regularization; tvdiff_parameters...)

X₁ = Xₙ'
Ẋ = Custom.calc_derivatives([V̇, Ẇ], Xₙ, Custom.parameters_values(params))'

# Define the problem
@named data_driven_problem = ContinuousDataDrivenProblem(X₁, times, Ẋ)

# Add the basis
@variables V, W
term_library = polynomial_basis([V, W], 4)
@named model_basis = Basis(term_library, [V, W])

# Discover the model per DSTLS
optimizerₙ = DSTLS(0.15)
solₙ = solve(data_driven_problem, model_basis, optimizerₙ)
basisₙ = get_basis(solₙ)
paramsₙ = get_parameter_map(basisₙ)

# And do the same per STLS
optimizerₒ = STLSQ(0.08)
solₒ = solve(data_driven_problem, model_basis, optimizerₒ)
basisₒ = get_basis(solₒ)
paramsₒ = get_parameter_map(basisₒ)

# And do the same per SR3
optimizerₛ = SR3(0.1, 1.0)
solₛ = solve(data_driven_problem, model_basis, optimizerₛ)
basisₛ = get_basis(solₛ)
paramsₛ = get_parameter_map(basisₛ)

# And do the same per ARDLS
optimizerₐ = ARDLS(0.02)
solₐ = solve(data_driven_problem, model_basis, optimizerₐ)
basisₐ = get_basis(solₐ)
paramsₐ = get_parameter_map(basisₐ)

tex_systemₙ = Custom.as_tex(basisₙ; environment="", line_length=85, lhs="\\dot{%s}", format_var="%s")
save(here("../generated/tex_system_dstls.tex"), tex_systemₙ)

tex_systemₒ = Custom.as_tex(basisₒ; environment="", line_length=85, lhs="\\dot{%s}", format_var="%s")
save(here("../generated/tex_system_stls.tex"), tex_systemₒ)

tex_systemₐ = Custom.as_tex(basisₐ; environment="", line_length=85, lhs="\\dot{%s}", format_var="%s")
save(here("../generated/tex_system_ardls.tex"), tex_systemₐ)
