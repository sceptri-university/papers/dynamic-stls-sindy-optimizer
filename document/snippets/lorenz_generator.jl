using Revise
using ModelingToolkit, DifferentialEquations
using CairoMakie, LaTeXStrings
using Random

# My own packages
include("../src/custom.jl")
using .Custom

Random.seed!(123)

# Setting up independent variable namely time t and dependent variable x(t), y(t), z(t)
@variables t x(t) y(t) z(t)
@parameters σ, ρ, β

# Our system is one ODE, so we only need differential with respect to time
D = Differential(t)

# Governing equation(s) for our system
ẋ(x, y, z, σ) = σ * (y - x)
ẏ(x, y, z, ρ) = x * (ρ - z) - y
ż(x, y, z, β) = x * y - β * z

params = [
    σ => 10,
    ρ => 28,
    β => 8 / 3
]
starting_values = [1, 1, 1]
time_range = (0.0, 30)

# Define ODESystem - more precisely the differential equation(s) that define it
@named ode_system = ODESystem([
    D(x) ~ ẋ(x, y, z, σ),
    D(y) ~ ẏ(x, y, z, ρ),
    D(z) ~ ż(x, y, z, β)
])

ode_problem = ODEProblem(ode_system, starting_values, time_range, params)

saveat = 0.01
solution = solve(ode_problem, Rosenbrock23(); saveat)

# For ease of handling, we create a temporary object to hide away saving logic
results = Custom.process_solution(
    solution,
    (:x, :y, :z), # names of variables
    (coords, t) -> [
        ẋ(coords..., last.(params)[1]),
        ẏ(coords..., last.(params)[2]),
        ż(coords..., last.(params)[3])
    ]
)