using Revise
using DataDrivenDiffEq, DataDrivenSparse
using ModelingToolkit, DifferentialEquations
using LinearAlgebra, Random
using CairoMakie, ColorSchemes, LaTeXStrings
using NoiseRobustDifferentiation

# My own packages
include("../src/custom.jl")
using .Custom, .Custom.Noiser
using DynamicSTLS

times = results.times
true_trajectory = results.trajectory'
true_derivative = results.derivatives'

# Clearing variables used for saving
begin
    regularization = 0
    λ = 0
    k = 1
end

Random.seed!(123)

noise = AdditiveVarianceNoise(0, 0.01)
noisy_trajectory = noise(true_trajectory)

λ = [0.3, 0.3, 0.3];
k = [4, 5, 5];
denoised_trajectory = denoise(noisy_trajectory, k, λ)

resolution = length(times)
time_length = times[end] - times[1]
each_step = time_length / resolution

tvdiff_parameters = (
    dx=each_step,
    # diff_kernel="square",
    scale="large",
    ε=1e-10,
)

number_of_iterations = [30, 30, 30]
regularization = [0.08, 0.9, 0.6]

derivative_df = tvdiff(noisy_trajectory, number_of_iterations, regularization; tvdiff_parameters...)

@named data_driven_problem = ContinuousDataDrivenProblem(
    denoised_trajectory,
    times,
    derivative_df
)

@variables x, y, z
# Variables present in our data (and their names)
variables = [x, y, z]
# Here we need to define what library of terms we will use for sparsification
term_library = polynomial_basis(variables, 3)

# Most commonly we will use `t` as independent variable (most often time)
@named model_basis = Basis(term_library, variables)

# Discover the model per DSTLS
optimizerₙ = DSTLS(0.03)
solₙ = solve(data_driven_problem, model_basis, optimizerₙ)
basisₙ = get_basis(solₙ)
paramsₙ = get_parameter_map(basisₙ)

# And do the same per STLS
optimizerₒ = STLSQ(0.1)
solₒ = solve(data_driven_problem, model_basis, optimizerₒ)
basisₒ = get_basis(solₒ)
paramsₒ = get_parameter_map(basisₒ)

# Creating an ODE System from our fitted model
starting_values = data_driven_problem.X[:, 1]
timespan = (data_driven_problem.t[begin], data_driven_problem.t[end])

# Compute prediction for DSTLS found model
@named predictiveₙ = ODESystem(
    equations(basisₙ),
    get_iv(basisₙ),
    states(basisₙ),
    parameters(basisₙ);
    checks=false
);

problemₙ = ODEProblem(predictiveₙ, starting_values, timespan, paramsₙ)
predictionₙ = solve(problemₙ, Rosenbrock23(); saveat=data_driven_problem.t)
prediction_trajectoryₙ = mapreduce(permutedims, vcat, predictionₙ.u)

# Compute prediction for STLS found model
@named predictiveₒ = ODESystem(
    equations(basisₒ),
    get_iv(basisₒ),
    states(basisₒ),
    parameters(basisₒ);
    checks=false
);

problemₒ = ODEProblem(predictiveₒ, starting_values, timespan, paramsₒ)
predictionₒ = solve(problemₒ, Rosenbrock23(); saveat=data_driven_problem.t)
prediction_trajectoryₒ = mapreduce(permutedims, vcat, predictionₒ.u)

figure = Custom.attractor_plot(prediction_trajectoryₒ', prediction_trajectoryₙ', 
	noisy_trajectory, results.variables;
	first_label=L"\text{STLS}",
    second_label=L"\text{DSTLS}",
	raw_label=L"\text{raw}",
	title="",
	raw_color=(:gray, 0.8),
	legend_below = true,
    resolution=(360, 350),
    colormap=:Zissou1Continuous,
	transform=false, flip=true
)