local function read_file(path)
	local open = io.open
    local file = open(path, "rb") -- r read mode and b binary mode
    if not file then return nil end
    local content = file:read "*a" -- *a or *all reads the whole file
    file:close()
    return content
end

function read_as_snippet(env, file)
	tex.print('\\begin{' .. env .. '}')
	tex.print(read_file('snippets/' .. file))
	tex.print('\\end{' .. env .. '}')
end
