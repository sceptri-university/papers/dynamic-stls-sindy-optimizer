"""
Additive noise with variance relative to the variance of data
```
D(noise) / D(data) = amplitude
```
Therefore distribution of the noise is given by
```
noise ~ (0, amplitude²)
```
"""
Base.@kwdef struct AdditiveVarianceNoise <: AbstractNoise
    μ::Float64 = 0
    σ̂::Float64 = 0.2
    args::Vector = []
    distribution::Type{<:Distribution} = Normal

    AdditiveVarianceNoise(μ, σ̂) = new(μ, σ̂, [], Normal)
    AdditiveVarianceNoise(μ, σ̂, args, distribution) = new(μ, σ̂, args, distribution)
end
function (noise::AdditiveVarianceNoise)(data::AbstractArray)
    additive_noise = AdditiveNoise(noise.distribution(
        noise.μ, sqrt(noise.σ̂) * std(data), noise.args...
    ))
    return additive_noise(data)
end

#>--OPTIONAL
import Base.show
Base.show(io::IO, noise::AdditiveVarianceNoise) = print(io, "AdditiveVarianceNoise($(noise.μ), $(noise.σ̂))")
#>--END