Base.@kwdef struct AdditiveNoise <: AbstractNoise
    distribution::Distribution = Normal(0, 0.5)

    AdditiveNoise(μ, σ) = new(Normal(μ, σ))
    AdditiveNoise(distribution::Distribution) = new(distribution)
end
(noise::AdditiveNoise)(data::AbstractArray) = data .+ rand(noise.distribution, size(data))

#>--OPTIONAL
import Base.show
Base.show(io::IO, noise::AdditiveNoise) = print(io, "AdditiveNoise($(noise.distribution.μ), $(noise.distribution.σ))")
#>--END