module DynamicSTLS
using DataDrivenDiffEq, DataDrivenSparse

include("DSTLS.jl")
include("ARDLS.jl")
export DSTLS, DSTLSCache
export ARDLS, ARDLSCache
export init_cache, step!

# Below are custom printing functions for Optimizer types
# which are required reproducibility of naming scheme

function Base.show(io::IO, opt::ADMM)
    args = [opt.thresholds]
    if opt.rho != 1.0
        push!(args, opt.rho)
    end

    args_s = join(args, ',')

    return print(io, "ADMM($args_s)")
end

function Base.show(io::IO, opt::STLSQ)
    args = [opt.thresholds]
    if opt.rho != 0.0
        push!(args, opt.rho)
    end

    args_s = join(args, ',')

    return print(io, "STLSQ($args_s)")
end

# Taken from here: https://docs.sciml.ai/DataDrivenDiffEq/stable/libs/datadrivensparse/sparse_regression/#DataDrivenSparse.SR3
function Base.show(io::IO, opt::SR3)
    args = [opt.proximal == HardThreshold() ? sqrt(2 * opt.thresholds) : opt.thresholds]
    if opt.nu != 1.0
        push!(args, opt.nu)
    end
    if opt.proximal != HardThreshold()
        push!(args, opt.proximal)
    end

    args_s = join(args, ',')

    return print(io, "SR3($(args_s))")
end

end # module DynamicSTLS