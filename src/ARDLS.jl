import DataDrivenSparse: init_cache, step!, get_thresholds, _is_converged, _set!
using LinearAlgebra, Statistics

struct ARDLS{T<:Real,TT<:Union{Number,AbstractVector}} <: DataDrivenSparse.AbstractSparseRegressionAlgorithm
    a::T
    b::T
    """Consider-zero threshold"""
    thresholds::TT


    function ARDLS(threshold::TT=1e-1, a::T=1e-10, b::T=a) where {T<:Real,TT}
        @assert all(threshold .> zero(eltype(threshold))) "Threshold must be positive definite"
        return new{T,TT}(a, b, threshold)
    end
end

Base.summary(::ARDLS) = "ARDLS"
Base.show(io::IO, opt::ARDLS) = print(io, "ARDLS(a=$(opt.a),b=$(opt.b),$(opt.thresholds))")

struct ARDLSCache{C<:AbstractArray,A<:BitArray,AT,AST,BT,ST,CT,FT,NT,SXT,aT,bT,ATT,BTT} <:
       DataDrivenSparse.AbstractSparseRegressionCache
    X::C
	Ξ::C
    Ξ_prev::C
    active_set::A
    # Scaled input data
    A::AT
    Aₛ::AST
    B::BT
    # Only cached data
    scale::ST
    covariance::CT
    factor::FT
    noise_variance::NT
    Sx::SXT
    # Algorithm data
    a::aT
    b::bT
    # Original Data
    Ã::ATT
    B̃::BTT
end

# _is_converged function must be re-implemented, because X and active_set should be ignored
# while iterating
function _is_converged(x::ARDLSCache, abstol, reltol)::Bool
    @unpack Ξ, Ξ_prev = x
    Δ = norm(Ξ .- Ξ_prev)
    Δ < abstol && return true
    δ = Δ / norm(Ξ)
    δ < reltol && return true
    return false
end

function _set!(x::ARDLSCache,
    y::ARDLSCache) where {T<:Number}
    begin
        foreach(eachindex(x.X)) do i
            x.X[i] = y.X[i]
			x.Ξ[i] = y.Ξ[i]
            x.Ξ_prev[i] = y.Ξ_prev[i]
            x.active_set[i] = y.active_set[i]
        end
        return
    end
end

function init_cache(alg::ARDLS, A::AbstractMatrix, b::AbstractVector)
    init_cache(alg, A, permutedims(b))
end

function init_cache(alg::ARDLS, A::AbstractMatrix, B::AbstractMatrix)
    @assert size(B, 1) == 1 "Caches only hold single targets!"

    Ã, B̃ = A, B

    Mₓ = maximum(abs.(A))
    A = A ./ Mₓ
    B = B ./ Mₓ

    scale = sqrt.(diag(A * A'))
    Aₛ = A' * diagm(1 ./ scale)

    covariance = Aₛ' * Aₛ
    factor = Aₛ' * B'

    Ξ = permutedims((covariance + maximum(covariance) / 100 * I) \ factor)

    residue = B' - Aₛ * Ξ'
    noise_variance = 1 / mean(residue .^ 2)
    Sx = diagm(abs.(view(Ξ, :)) .* 0.1 .+ 0.01 .* mean(Ξ))

    Ξ ./= scale'
    Ξ_prev = zeros(size(Ξ))

    active_set = BitArray(undef, size(Ξ))
    λ = minimum(DataDrivenSparse.get_thresholds(alg))

	X = copy(Ξ)
    active_set .= abs.(X) .> λ
    X[[!i for i in active_set]] .= 0

    return ARDLSCache{typeof(Ξ),typeof(active_set),typeof(A),typeof(Aₛ),typeof(B),typeof(scale),
        typeof(covariance),typeof(factor),typeof(noise_variance),typeof(Sx),typeof(alg.a),typeof(alg.b),typeof(Ã),
        typeof(B̃)}(X, Ξ, Ξ_prev, active_set, A, Aₛ, B, scale, covariance, factor, noise_variance, Sx, alg.a, alg.b, Ã, B̃)
end

function step!(cache::ARDLSCache, λ)
    @unpack X, Ξ, Ξ_prev, A, Aₛ, B, scale, covariance, factor, noise_variance, Sx, a, b, active_set = cache

    correction = diagm((a + 0.5) ./ (b .+ 0.5 * (view(Ξ, :) .* scale .^ 2 .+ diag(Sx))))
    Sx = inv(noise_variance * covariance + correction)

    Ξ .= noise_variance * factor' * Sx'

    residue = B' - Aₛ * Ξ'
    noise_variance = size(Aₛ, 1) / (dot(residue, residue) + tr(Sx * covariance))

    Ξ ./= scale'

	# Zeroing small coefficients too early changes the result real bad
    Ξ_prev .=Ξ
	X .= Ξ

    active_set .= abs.(X) .> λ
    X[[!i for i in active_set]] .= 0
end