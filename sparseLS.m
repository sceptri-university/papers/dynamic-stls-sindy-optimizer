function [x,hist]=sparseLS(A,y,POS,init);
% function [x,hist]=sparseLS(A,y);
% function returns variational estimates of least square problem
%
%    A x = y  + noise
%
% A    - is the matrix of linear combinations
% y    - is the vector of outputs
% POS  - if set to 1, positivity of x is assumed
%
% White Gaussian noise is assumed, i.e. e is from N(0,omega*eye()).
% If the noise is realative to the measurements, the input matrix can be scaled.
%             S*A*x = S*y + S*e
% where S is the square root of the inverse covariance matrix of the noise
%
% nargout: x        - estimate of x
%          hist     - history of internal variables
%


if nargin<3,
    POS	= 0;
end

if nargin<4
    if POS
        init = 1;
    else
        init = 1;
    end
end


if nargout<2
    HIST	= 0;
else
    HIST	= 1;
end

% prevent accidental input in the wrong order
if size(A,2)>1000
    error('check dimension of matrix A''A');
end

% prescale norm(y)<=1 to make stopping constants meaningful.
my = max(abs(y));
A = A/my;
y = y/my;

% make parameters comparable
scaleA = sqrt(diag(A'*A));
A = A*diag(1./scaleA);

nx = size(A,2);
ny = size(A,1);
% init
AA = A'*A;
Ay = A'*y;
switch init 
    case 0
        tol = max(eig(AA))/1000;
        x = pinv(AA,tol)* Ay;
    %    x = mean(x)*ones(n,1);
    case 1
        x = inv(AA+max(max(AA))/100*eye(nx))* Ay;
    case 2
        x = lsqnonneg(A,y);
end

disp(sprintf('Answer: (%d, %d, %d)\n', x))

res = y - A*x; % residue
om = 1/mean(res.^2); % variance of the noise
Sx = diag(abs(x)*0.1+0.01*mean(x));

%% hist
if HIST
    hist.x	= ones(nx,10);
    hist.x(:,1) = x;
    hist.sx	= ones(nx,10);
    hist.sx(:,1) = diag(Sx);
    hist.om	= ones(1,10);
    hist.om(1) = om;
end

a	= 1e-10;
b	= a;

iterate	= 1;
it	= 1;
old_om = om;

while iterate & (it<10)

    Omx = diag((a+0.5)./(b+0.5*(x.^2+diag(Sx))));

    %     Omx = (n*(a+0.5))./sum(0.5*(x.^2+diag(Sx)))*eye(nx);
    %     keyboard

    Sx = inv(om*AA+Omx);
    x = om*Sx*Ay;
    disp(sprintf('Answer: (%d, %d, %d)\n', x))
    if POS
        sigx = sqrt(diag(Sx));
        [nx,vx] = momtrun_low(x,sigx,0);
        nsx = sqrt(vx - nx.^2);
        rati = nsx./sigx;
        R = diag( rati );
        Sx = R*Sx*R;
        x = nx;
    end

    res = y - A*x;
    om = ny/(res'*res+ trace(Sx*AA));
    %    Om = 1./(res.^2 + diag(A*Sx*A')+1e-5);

    it = it+1;
    if HIST
        hist.x(:,it) = x;
        hist.sx(:,it) = diag(Sx);
        hist.om(it) = om;
        hist.omx(it,:) = diag(Omx);
    end

    %     om = min(Om);
    if abs(old_om - om)< om/1e4
        iterate = 1;
    end
    old_om = om;
end

x = x./scaleA;

% keyboard