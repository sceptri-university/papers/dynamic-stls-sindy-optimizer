N = 100
Y = [
    (1:N).^2;
    (N:-1:1).^2
    
]'
Ydot = [
    -2*ones(1,N);
    2*ones(1,N)
]'

bY = [
    ones(1,N);
    Y'
]'

xi1 = sparseLS(bY, Ydot(:,1))
%xi2 = sparseLS(bY, Ydot(:,2))